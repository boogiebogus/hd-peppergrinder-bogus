
//=================================================================
//"Zorcher" Portable Teleportation Device
//=================================================================
//A curious handmade tool recovered from the corpse of a cereal miner found in the caverns of Bazoik. 
//It can teleport anything, anywhere, an impressive technical feat, but whoever built it was either strapped for time or simply lacking common sense.
//With no means of configuring the destination and only the barest minimum of safety features, it's not about to revolutionize transportation.
//While it can teleport living beings without any apparent bodily harm, there's no guarantee that they won't find themselves falling out of the sky, stranded in a wasteland, or smack in the middle of an enemy ambush.
//With that in mind, it's almost a weapon. Our enemies may not so susceptible to environmental hazards, but displacing high-value targets in their formations could prove effective.

const HDLD_ZORCHER="zrc";

class HDZorcher:HDCellWeapon{
	default{
		//$Category "Weapons/Hideous Destructor"
		//$Title "Zorcher"
		//$Sprite "ZORPA0"

		+hdweapon.fitsinbackpack
		weapon.selectionorder 70;
		weapon.slotnumber 1;
		weapon.slotpriority 1;
		weapon.ammouse 1;
		scale 0.6;
		inventory.pickupmessage "You got the Zorcher handheld teleporter!";
		obituary "%o got killed by %k with a weapon that does no damage whatsoever.";
		hdweapon.barrelsize 24,1.2,2;
		hdweapon.refid HDLD_ZORCHER;
		tag "Zorcher";

	}
	override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
	override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
	override double gunmass(){
		return 8+(weaponstatus[ZORS_BATTERY]<0?0:2);
	}
	override double weaponbulk(){
		return 75+(weaponstatus[1]>=0?ENC_BATTERY_LOADED:0);
	}
	override string,double getpickupsprite(){return "ZORPA0",1.;}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			sb.drawbattery(-54,-4,sb.DI_SCREEN_CENTER_BOTTOM,reloadorder:true);
			sb.drawnum(hpl.countinv("HDBattery"),-46,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		}
		int bat=hdw.weaponstatus[ZORS_BATTERY];
		if(bat>0)sb.drawwepnum(bat,20);
		else if(!bat)sb.drawstring(
			sb.mamountfont,"00000",
			(-16,-9),sb.DI_TEXT_ALIGN_RIGHT|sb.DI_TRANSLATABLE|sb.DI_SCREEN_CENTER_BOTTOM,
			Font.CR_DARKGRAY
		);
	}
	override string gethelptext(){
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_RELOADRELOAD
		..WEPHELP_UNLOADUNLOAD
		;
	}

	override void failedpickupunload(){
		failedpickupunloadmag(ZORS_BATTERY,"HDBattery");
	}
	override void consolidate(){
		CheckBFGCharge(ZORS_BATTERY);
	}
	
	action void A_FireZorcher(){
        vector3 gpos=pos+gunpos((0,0,-getdefaultbytype("ZorcherBall").height*0.6));
		let ggg=zorcherball(spawn("ZorcherBall",gpos,ALLOW_REPLACE));
		ggg.angle=angle;ggg.pitch=pitch;ggg.target=self;ggg.master=self;
        ggg.vel=vel+(cos(pitch)*(cos(angle),sin(angle)),-sin(pitch))*ggg.speed;
        invoker.weaponstatus[ZORS_BATTERY]=0;
        A_StartSound("weapons/zorcfire", CHAN_WEAPON);
	}
	
	states{
	ready:
		ZRCH A 1 A_WeaponReady(WRF_ALL&~WRF_ALLOWUSER1);
		goto readyend;
	fire:
		#### A 0 offset(0,35) A_JumpIf(invoker.weaponstatus[ZORS_BATTERY]<20, "nope");
	shoot:
        #### B 2 A_GunFlash();
		#### B 2 offset(1,40) A_FireZorcher();
		#### C 2 offset(0,48) A_WeaponReady(WRF_NONE);
		#### C 2 offset(-1,40) A_WeaponReady(WRF_NONE);
		#### AAA 4 offset(0,32) A_WeaponReady(WRF_NONE);
		goto ready;
	flash:
		TNT1 A 0;
		#### A 1 bright{
			HDFlashAlpha(64);
			A_Light2();
		}
		#### AA 1 bright;
		#### A 1 bright A_Light1();
		#### AA 1 bright;
		#### B 0 bright A_Light0();
		stop;
	select0:
		ZRCH A 0;
		goto select0big;
	deselect0:
		ZRCH A 0 A_Light0();
		goto deselect0big;

	unload:
		#### A 0{
			invoker.weaponstatus[0]|=ZORF_JUSTUNLOAD;
			if(invoker.weaponstatus[ZORS_BATTERY]>=0)
				return resolvestate("unmag");
			return resolvestate("nope");
		}goto magout;
	unmag:
		#### A 2 offset(0,33){
			A_SetCrosshair(21);
			A_MuzzleClimb(frandom(-1.2,-2.4),frandom(1.2,2.4));
		}
		#### A 3 offset(0,35) A_StartSound("weapons/zorcopen",8);
		#### A 6 offset(0,40) A_StartSound("weapons/zorcload",8,CHANF_OVERLAP);
		#### A 0{
			int bat=invoker.weaponstatus[ZORS_BATTERY];
			A_MuzzleClimb(frandom(-1.2,-2.4),frandom(1.2,2.4));
			if(
				(
					bat<0
				)||(
					!PressingUnload()&&!PressingReload()
				)
			)return resolvestate("dropmag");
			return resolvestate("pocketmag");
		}

	dropmag:
		---- A 0{
			int bat=invoker.weaponstatus[ZORS_BATTERY];
			invoker.weaponstatus[ZORS_BATTERY]=-1;
			if(bat>=0){
				HDMagAmmo.SpawnMag(self,"HDBattery",bat);
			}
		}goto magout;

	pocketmag:
		---- A 0{
			int bat=invoker.weaponstatus[ZORS_BATTERY];
			invoker.weaponstatus[ZORS_BATTERY]=-1;
			if(bat>=0){
				HDMagAmmo.GiveMag(self,"HDBattery",bat);
			}
		}
		#### A 8 offset(0,43) A_StartSound("weapons/pocket",9);
		#### A 8 offset(0,42) A_StartSound("weapons/pocket",9,CHANF_OVERLAP);
		goto magout;

	magout:
		---- A 0 A_JumpIf(invoker.weaponstatus[0]&ZORF_JUSTUNLOAD,"reload3");
		goto loadmag;

	reload:
		#### A 0{
			invoker.weaponstatus[0]&=~ZORF_JUSTUNLOAD;
			if(
				invoker.weaponstatus[ZORS_BATTERY]<20
				&&countinv("HDBattery")
			)setweaponstate("unmag");
		}goto nope;

	loadmag:
		#### A 2 offset(0,43){
			A_StartSound("weapons/pocket",9);
			if(health>39)A_SetTics(0);
		}
		#### AA 2 offset(0,42);
		#### A 2 offset(0,44) A_StartSound("weapons/pocket",9);
		#### A 4 offset(0,43);
		#### A 6 offset(0,42);
		#### A 8 offset(0,38)A_StartSound("weapons/zorcload",8);
		#### A 4 offset(0,37){if(health>39)A_SetTics(0);}
		#### A 4 offset(0,36)A_StartSound("weapons/zorcclose",8);

		#### A 0{
			let mmm=HDMagAmmo(findinventory("HDBattery"));
			if(mmm)invoker.weaponstatus[ZORS_BATTERY]=mmm.TakeMag(true);
		}goto reload3;

	reload3:
		#### A 6 offset(0,40){
			A_StartSound("weapons/zorcclose2",8);
		}
		#### A 2 offset(0,36);
		#### A 4 offset(0,33);
		goto nope;

	user3:
		#### A 0 A_MagManager("HDBattery");
		goto ready;

	spawn:
		ZORP A -1;
		stop;
	}
	override void initializewepstats(bool idfa){
		weaponstatus[ZORS_BATTERY]=20;
	}
}
enum zorcherstatus{
	ZORF_JUSTUNLOAD=1,

	ZORS_FLAGS=0,
	ZORS_BATTERY=1,
};


class Zorcherball:HDFireball{
	default{
		+extremedeath
		damagetype "balefire";
		activesound "cyber/ballhum";
		//seesound "weapons/plasmaf";
		decal "scorch";
		gravity 0;
		height 6;radius 4;
		speed 70;
		scale 0.15;
		damagefunction(0);
        translation "112:126=176:190";
	}
	actor lite;
	string pcol;
	override void postbeginplay(){
		super.postbeginplay();
		lite=spawn("ZorcherBallLight",pos,ALLOW_REPLACE);lite.target=self;
		pcol="ff 55 55";
	}
	states{
	spawn:
		BFS1 A 0{
			if(stamina>40||!target||target.health<1)return;  
			stamina++;
			actor tgt=target.target;
			if(getage()>144)vel+=(frandom(-0.3,0.3),frandom(-0.3,0.3),frandom(0.1,-0.3));
		}
		BFS1 ABAB 1 bright{
			for(int i=0;i<10;i++){
				A_SpawnParticle(pcol,SPF_RELATIVE|SPF_FULLBRIGHT,35,frandom(1,4),0,
					frandom(-8,8)-5*cos(pitch),frandom(-8,8),frandom(0,8)+sin(pitch)*5,
					frandom(-1,1),frandom(-1,1),frandom(1,2),
					-0.1,frandom(-0.1,0.1),-0.05
				);
			}
			scale=(1,1)*frandom(0.35,0.45);
		}loop;
	death:
		BFE1 A 1 bright{
			spawn("HDSmoke",pos,ALLOW_REPLACE);
			A_StartSound("weapons/zorcx",CHAN_BODY,CHANF_OVERLAP,volume:0.4);
			damagetype="hot";
			bextremedeath=false;
			//A_Explode(64,64);
			if(lite)lite.args[3]=128;
			DistantQuaker.Quake(self,2,35,512,10);

			//hit map geometry
			if(
				blockingline||
				floorz>=pos.z||  
				ceilingz-height<=pos.z
			){
				A_SpawnChunks("HDSmoke",3,2,3);
				A_SpawnChunks("HugeWallChunk",50,4,20);
			}

			//teleport victim
			if(
				blockingmobj
				/*
				&&!blockingmobj.player
				&&!blockingmobj.special
				&&(
					!blockingmobj.bismonster
					||blockingmobj.health<1
				)
				&&!random(0,3)*/
			){
				spawn("TeleFogRed",blockingmobj.pos,ALLOW_REPLACE);
				vector3 teleportedto=(
                    frandom(-20000,20000),
					frandom(-20000,20000),
					frandom(-20000,20000)
					);
        
				
				while(!level.ispointinlevel(teleportedto))teleportedto=(
					frandom(-20000,20000),
					frandom(-20000,20000),
					frandom(-20000,20000)
				);
				tracer.setorigin(teleportedto,false);
				tracer.setz(clamp(tracer.pos.z,tracer.floorz,tracer.ceilingz-tracer.height));
				tracer.vel=(frandom(-10,10),frandom(-10,10),frandom(10,20));
				spawn("TeleFogRed",tracer.pos,ALLOW_REPLACE);
			}
		}
		BFE1 BBCDDEEE 2 bright A_FadeOut(0.05);
		stop;
	}
}
class ZorcherBallLight:PointLight{
	override void postbeginplay(){
		super.postbeginplay();
		args[0]=206;
		//bool freedoom=(Wads.CheckNumForName("FREEDOOM",0)!=-1);
		args[1]=52;
		args[2]=48;
		args[3]=0;
		args[4]=0;
	}
	override void tick(){
		if(!target){
			args[3]+=random(-10,1);
			if(args[3]<1)destroy();
		}else{
			if(target.bmissile)args[3]=random(32,40);
			else args[3]=random(48,64);
			setorigin(target.pos,true);
		}
	}
}

class TeleFogRed:IdleDummy{
	default{
		renderstyle "add";alpha 0.6;
        translation "112:126=176:190"; 
	}
	override void postbeginplay(){
		actor.postbeginplay();
		scale.x*=randompick(-1,1);
		A_StartSound("misc/zorcport");
	}
	override void A_ShardSuck(vector3 aop,int range,bool forcegreen){
		actor a=spawn("FragShardRed",aop,ALLOW_REPLACE);
		a.setorigin(aop+(random(-range,range)*6,random(-range,range)*6,random(-range,range)*6),false);
		a.vel=(aop-a.pos)*0.05;
		a.stamina=20;
		if(forcegreen)a.A_SetTranslation("AllGreen");
	}
	states{
	spawn:
		TFOG AA 2 nodelay bright light("ZLS1") A_FadeIn(0.2);
		TFOG BBCCCDDEEFGHII 2 bright light("ZLS1"){
			A_ShardSuck(pos+(0,0,frandom(24,48)),4,forcegreen:false);
		}
		TFOG JJJJ random(2,3) bright light("ZLS1"){
			alpha-=0.2;
			A_ShardSuck(pos+(0,0,frandom(24,48)),4,forcegreen:false);
		}stop;
	nope:
		TNT1 A 20 light("ZLS1");
		stop;
	}
}

class FragShardRed:FragShard{
	default{
        translation "112:126=176:190";
	}
}
