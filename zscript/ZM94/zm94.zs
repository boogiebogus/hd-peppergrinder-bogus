const HDLD_ZM94 = "z94";
const HDLD_ZM94MAG = "505";

const ENC_ZM94MAG_EMPTY = 27;
const ENC_ZM94MAG_LOADED = 16;

class ZM94Rifle : HDWeapon{
	default{
		//$Category "Weapons/Hideous Destructor"
		//$Title "ZM94"
		//$Sprite "ZM94A0"

		weapon.slotnumber 8;
		weapon.slotpriority 0.5;
		weapon.kickback 50;
		weapon.selectionorder 27;
		inventory.pickupsound "misc/w_pkup";
		inventory.pickupmessage "You got the ZM94 \'Sabrewolf\' anti-materiel rifle!";
		weapon.bobrangex 0.22;
		weapon.bobrangey 0.9;
		scale 0.8;
		obituary "%o was forcefully penetrated by %k.";
		hdweapon.refid HDLD_ZM94;
		tag "ZM94 \'Sabrewolf\' AMR";
		inventory.icon "ZM94A0";
        
        hdweapon.loadoutcodes "
			\cualtreticle - 0/1, whether to use the glowing crosshair
			\cufrontreticle - 0/1, whether crosshair scales with zoom
			\cubulletdrop - 0-600, amount of compensation for bullet drop
			\cuzoom - 4-70 (15-40 if frontreticle), 10x the resulting FOV in degrees";

	}
	override string pickupmessage(){
		string msg=super.pickupmessage();
		int bc=weaponstatus[Z94S_BREAKCHANCE];
		if(bc>50){
			msg.replace("!","");
			msg.replace("the","an");
		}
        if(!bc)msg=msg.." It's in pristine shape.";
		else if(bc>200)msg=msg..". It might be good for a shot or two.";
		else if(bc>100)msg=msg..". It's been through a lot.";
		else if(bc>50)msg=msg..". It's getting a bit worn.";
		return msg;
	}
	override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
	override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
	override void postbeginplay(){
		super.postbeginplay();
		
		barrelwidth=1;
		barreldepth=3;
		barrellength=29;
        weaponspecial=1337;
	}
	override double gunmass(){
		double howmuch=17;
		return howmuch+weaponstatus[Z94S_MAG]*0.04;
	}
	override double weaponbulk(){
		double blx = 125;
		
		int mgg=weaponstatus[Z94S_MAG];
		int stw = (weaponstatus[0] & Z94F_STOWED?25:0);
		return blx+(mgg<0?0:(ENC_ZM94MAG_LOADED+mgg*(ENC_50OMG_LOADED*0.75))+stw);
	}
	override string,double getpickupsprite(){
		string spr;
		// A: +m -auto
		// B: -m -auto
		// C: +m +auto
		// D: -m +auto
		
		if (!(weaponstatus[0] & Z94F_STOWED))
		{
			if (weaponstatus[Z94S_MAG]<0)
				spr = "D";
			else 
				spr = "C";
		}
		else
		{
			if (weaponstatus[Z94S_MAG]<0)
				spr = "B";
			else 
				spr = "A";
		}
		
		return "ZM94"..spr.."0",1.;
	}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			int nextmagloaded=sb.GetNextLoadMag(hdmagammo(hpl.findinventory("HDZM94Mag")));
			if(nextmagloaded>=5){
				sb.drawimage("OMG5A0",(-50,-3),sb.DI_SCREEN_CENTER_BOTTOM,scale:(1.6,1.6));
			}else if(nextmagloaded<1){
				sb.drawimage("OMG5B0",(-50,-3),sb.DI_SCREEN_CENTER_BOTTOM,alpha:nextmagloaded?0.6:1.,scale:(1.6,1.6));
			}else sb.drawbar(
				"94MGNORM","94MGGREY",
				nextmagloaded,5,
				(-50,-3),-1,
				sb.SHADER_VERT,sb.DI_SCREEN_CENTER_BOTTOM
			);
			sb.drawnum(hpl.countinv("HDZM94Mag"),-43,-8,sb.DI_SCREEN_CENTER_BOTTOM,font.CR_BLACK);
		}
		
		int lod=max(hdw.weaponstatus[Z94S_MAG],0);
		sb.drawwepnum(lod,5);
		if (hdw.weaponstatus[0] & Z94F_ISJAMMED)
		{
			sb.drawwepdot(-16, -10, (1, 3));
			lod++;
		}
		else if (hdw.weaponstatus[Z94S_CHAMBER]==2)
		{
			sb.drawwepdot(-16, -10, (5, 1));
			lod++;
		}
		
		
			sb.drawwepcounter(hdw.weaponstatus[Z94S_AUTO],
			-22,-10,"RBRSA3A7","STFULAUT","STBURAUT");
              
	}
	override string gethelptext(){
		bool gl = false;
		bool glmode = gl;
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_ALTFIRE.."  Clear jam (if any)\n"
		..WEPHELP_RELOAD.."  Reload mag\n"
		..WEPHELP_MAGMANAGER
		..WEPHELP_UNLOAD.."  Unload ".."magazine\n"
		..WEPHELP_ZOOM.." + "..WEPHELP_UNLOAD.." Repair\n"
		;
	}
	
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
		{
			int cx,cy,cw,ch;
			[cx,cy,cw,ch]=Screen.GetClipRect();
			sb.SetClipRect(
				-16+bob.x,-4+bob.y,32,16,
				sb.DI_SCREEN_CENTER
			);
			vector2 bobb = bob * 1.1;
			bobb.y=clamp(bobb.y,-8,8);
			sb.drawimage(
				"frntsite",(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
				alpha:0.9,scale:(1.2,2)
			);
			sb.SetClipRect(cx,cy,cw,ch);
			sb.drawimage(
				"backsite",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
				scale:(1.4,1.2)
			);
			if(scopeview){
				int scaledyoffset=54;
				int scaledwidth=72;
				double degree=hdw.weaponstatus[LIBS_ZOOM]*0.1;
				double deg=1/degree;
				int cx,cy,cw,ch;
				[cx,cy,cw,ch]=screen.GetClipRect();
				sb.SetClipRect(
					-36+bob.x,21+bob.y,scaledwidth,scaledwidth,
					sb.DI_SCREEN_CENTER
				);

					//the new circular view code that doesn't work with LZDoom 3.87c

					sb.fill(color(255,0,0,0),
						bob.x-36,scaledyoffset+bob.y-33,
						72,72,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER
					);

					texman.setcameratotexture(hpc,"HDXCAM_LIB",degree);
					let cam  = texman.CheckForTexture("HDXCAM_LIB",TexMan.Type_Any);
					let reticle = texman.CheckForTexture(
						(hdw.weaponstatus[0] & Z94F_ALTRETICLE)? "reticle2" : "reticle1"
					,TexMan.Type_Any);

					vector2 frontoffs=(0,scaledyoffset+3)+bob*2;

					double camSize = texman.GetSize(cam);
					sb.DrawCircle(cam,frontoffs,.08825,usePixelRatio:true);

					let reticleScale = camSize / texman.GetSize(reticle);
					if(hdw.weaponstatus[0]&Z94F_FRONTRETICLE){
						sb.DrawCircle(reticle,frontoffs,393*reticleScale, bob*4, 1.6*deg);
					}else{
						sb.DrawCircle(reticle,(0,scaledyoffset+3)+bob,.403*reticleScale, uvScale: .52);
					}

					//see comments in zm66.zs
					//let hole = texman.CheckForTexture("scophole",TexMan.Type_Any);
					//let holeScale    = camSize / texman.GetSize(hole);
					//sb.DrawCircle(hole, (0, scaledyoffset) + bob, .403 * holeScale, bob * 5, uvScale: .95);
				
				screen.SetClipRect(cx,cy,cw,ch);

				sb.drawimage(
					"libscope",(0,scaledyoffset+3)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER
				);
				
				sb.drawstring(
					sb.mAmountFont,string.format("%.1f",degree),
					(6+bob.x,92+bob.y),sb.DI_SCREEN_CENTER|sb.DI_TEXT_ALIGN_RIGHT,
					Font.CR_BLACK
				);
				sb.drawstring(
					sb.mAmountFont,string.format("%i",hdw.weaponstatus[Z94S_DROPADJUST]),
					(6+bob.x,16+bob.y),sb.DI_SCREEN_CENTER|sb.DI_TEXT_ALIGN_RIGHT,
					Font.CR_BLACK
				);
			}
		}
	}
	
	override void failedpickupunload(){
		failedpickupunloadmag(Z94S_MAG,"HDZM94Mag");
	}
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,1,10);
			if(owner.countinv("HD50OMGAmmo"))owner.A_DropInventory("HD50OMGAmmo",20);
			else{
				int angchange = 0;
				if(angchange)owner.angle-=angchange;
				owner.A_DropInventory("HDZM94Mag",1);
			}
		}
	}
	override void ForceBasicAmmo(){
		owner.A_TakeInventory("HD50OMGAmmo");
		owner.A_TakeInventory("HDZM94Mag");
		owner.A_GiveInventory("HDZM94Mag");
	}
	override void tick(){
		super.tick();
		drainheat(Z94S_HEAT,8);
	}
	action void A_EjectBrass(bool nojam = false)
	{
		bool success;
		
		actor brsss = null;
		if(invoker.weaponstatus[Z94S_CHAMBER]==1)
		{
			int jamchance;
			if (invoker.weaponstatus[Z94S_AUTO] > 0)
			{
				jamchance = 100 - (invoker.weaponstatus[Z94S_BREAKCHANCE]);
			}
			else
			{
				jamchance = 4000 - (invoker.weaponstatus[Z94S_BREAKCHANCE]);
			}
			
			if (!random(0, jamchance) && !nojam)
			{
				success = false;
				invoker.weaponstatus[0] |= Z94F_ISJAMMED;
				invoker.weaponstatus[Z94S_JAMFUCK] = 100;
				
				if (!random(0, 8))
				{
					invoker.weaponstatus[Z94S_JAMFUCK] += random(200, 300);
				}
				
				invoker.weaponstatus[Z94S_JAMFUCK] += random(0, 40);
			}
			else
			{
				double cosp = cos(pitch);
				[cosp,brsss]=A_SpawnItemEx("HDSpent50OMG",
					cosp*6,1,height-8-sin(pitch)*6,
					cosp*2,1,1-sin(pitch),
					0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
				brsss.vel += vel;
				brsss.A_StartSound(brsss.bouncesound,CHAN_BODY, volume: 0.4);
				
				success = true;
			}
		}
		else if(invoker.weaponstatus[Z94S_CHAMBER]==2)
		{
			double fc=max(pitch*0.01,5);
			double cosp=cos(pitch);
			[cosp,brsss]=A_SpawnItemEx("HDLoose50OMG",
				cosp*12,1,height-8-sin(pitch)*12,
				cosp*fc,0.2*randompick(-1,1),-sin(pitch)*fc,
				0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
			);
			brsss.vel+=vel;
			brsss.A_StartSound(brsss.bouncesound,CHAN_BODY, volume: 0.4);
			
			success = true;
		}
		
		if (success)
		{
			invoker.weaponstatus[Z94S_CHAMBER] = 0;
		}
	}
	
	action void A_Chamber(bool unloadonly = false, bool nojam = false)
	{
		A_StartSound("weapons/libchamber",6, volume: 0.3);
		
		if (!(invoker.weaponstatus[0] & Z94F_ISJAMMED))
		{
			if (invoker.weaponstatus[Z94S_CHAMBER] != 0)
			{
				A_EjectBrass(nojam);
			}
			
			if(!unloadonly && invoker.weaponstatus[Z94S_MAG]>0)
			{
				int jamchance;
				if (invoker.weaponstatus[Z94S_AUTO] > 0)
				{
					jamchance = 100 - (invoker.weaponstatus[Z94S_BREAKCHANCE]);
				}
				
				else
				{
					jamchance = 3000 - (invoker.weaponstatus[Z94S_BREAKCHANCE]);
				}
				
				if (random(0, jamchance) || nojam)
				{
					invoker.weaponstatus[Z94S_MAG]--;
					invoker.weaponstatus[Z94S_CHAMBER] = 2;
				}
			}
		}
	}
	
    override void consolidate(){
		if(weaponstatus[Z94S_BREAKCHANCE]>0){
			int bc=weaponstatus[Z94S_BREAKCHANCE];
			int oldbc=bc;
			weaponstatus[Z94S_BREAKCHANCE]=random(bc*2/3,bc);
			if(!owner)return;
			string msg="You try to unwarp some of the parts of your Sabrewolf";
			if(bc>oldbc)msg=msg..", but only made things worse.";
			else if(bc<oldbc*9/10)msg=msg..". It seems to work more smoothly now.";
			else msg=msg..", to little if any avail.";
			owner.A_Log(msg,true);
		}
	}
	
    override inventory createtossable(){
		let ctt=ZM94Rifle(super.createtossable());
		if(!ctt)return null;
		if(ctt.bmissile)ctt.weaponstatus[Z94S_BREAKCHANCE]+=random(0,10);
		return ctt;
	}

	
    action void VulcRepairMsg(){
		static const string vverbs[]={"remove some","buff out","realign","secure","grease","grab a spare part to replace","fiddle around and eventually suspect a problem with","forcibly un-warp","reassemble"};
		static const string vdebris[]={"debris","grease","dust","steel filings","powder","blood","pus","hair","dead insects","blueberry jam","cheese puff powder","tiny Bosses","Shapes"};
		static const string vpart[]={"barrel recoil spring","bolt roller bearing","dust cover","barrel locking mechanism","bipod","cylinder","cylinder feed port","pulley wire","pulley wheel","gas system"};
		static const string vpart2[]={"barrel feed port","chamber","extractor","extruder","barrel feed port seal","barrel","muzzle brake","firing pin","bolt"};

		string msg="You ";
		if(!random(0,3))msg=msg.."attempt to ";
		int which=random(0,vverbs.size()-1);
		msg=msg..vverbs[which].." ";
		if(!which)msg=msg..vdebris[abs(random(1,vdebris.size())-random(1,vdebris.size()))].." from ";
		msg=msg.."the ";

		which=random(0,vpart.size());
		if(which==vpart.size())msg=msg..vpart2[random(0,vpart2.size()-1)];
		else msg=msg..vpart[which];

		A_Log(msg..".",true);
	}

	
	states
	{
	select0:
		Z94G A 0{
            if((invoker.weaponstatus[0] & Z94F_STOWED))
            {
                setweaponstate("select0big");
            }
        }
		goto select0bfg;
	deselect0:
		Z94G A 0;
		goto deselect0bfg;
	ready:
        Z94G A 0{
            invoker.weaponstatus[0] |= Z94F_STOWED;
        }
		Z94G A 1{
			if(pressingzoom()){
				if(player.cmd.buttons&BT_USE){
					A_ZoomAdjust(Z94S_DROPADJUST,0,600,BT_USE);
				}else if(invoker.weaponstatus[0]&Z94F_FRONTRETICLE)A_ZoomAdjust(Z94S_ZOOM,15,40);
				else A_ZoomAdjust(Z94S_ZOOM,4,70);
				A_WeaponReady(WRF_NONE);
			}else A_WeaponReady(WRF_ALL);
            if(invoker.weaponstatus[Z94S_AUTO]>2)invoker.weaponstatus[Z94S_AUTO]=2;
		}goto readyend;
	user3:
		---- A 0;
		//goto super::user3;
		---- A 0 A_MagManager("HDZM94Mag");
		goto ready;

	fire:
		Z94G A 0
		{
			setweaponstate("firegun");
		}
	hold:
		Z94G A 1
		{
			if ((invoker.weaponstatus[Z94S_AUTO] > 3)
                || (invoker.weaponstatus[Z94S_AUTO] < 1)
				|| invoker.weaponstatus[Z94S_CHAMBER] != 2)
			setweaponstate("nope");
		}
		goto shoot;

	firegun:
		Z94G A 1
		{
            int trigger=invoker.weaponstatus[Z94S_AUTO];
            A_SetTics(1+trigger);
        }
	shoot:
		Z94G A 0
		{
			if(invoker.weaponstatus[Z94S_CHAMBER]==2)
				A_Gunflash();
			else
				setweaponstate("chamber_manual");
				
			A_WeaponReady(WRF_NONE);
		}
		Z94G B 1 A_EjectBrass();
		Z94G A 4
		{
			if (invoker.weaponstatus[Z94S_AUTO] == 1)
			{
				A_SetTics(3);
			}
			if (invoker.weaponstatus[Z94S_AUTO] > 1)
            {
                A_SetTics(0);
            }
		}
		Z94G A 0 {
            if(invoker.weaponstatus[Z94S_AUTO]>=2)invoker.weaponstatus[Z94S_AUTO]++;
        }
		Z94G A 0 A_Chamber();
		Z94G A 0 A_Refire();
		goto nope;
	flash:	
		Z69F A 1 bright{
			A_Light1();
			A_StartSound("weapons/zm94fire",1);

			HDBulletActor.FireBullet(self,"HDB_50OMG",
				aimoffy:(-1./600.)*invoker.weaponstatus[Z94S_DROPADJUST]
			);
			
            if(invoker.weaponstatus[Z94S_AUTO] > 0 && random(0,7))
            {
                invoker.weaponstatus[Z94S_BREAKCHANCE]++;
            }
            
			{
                if(gunbraced() && invoker.owner.player.crouchfactor <= 0.5)
                {
                    givebody(max(0,6-health));
                    damagemobj(invoker,self,5,"bashing");
                }
				HDFlashAlpha(32);
				A_ZoomRecoil(0.95);
				A_MuzzleClimb(
					0,0,
					0,0,
					-frandom(0.7,1.3),-frandom(4.0,4.8),
					-frandom(0.7,1.3),-frandom(4.0,4.8)
				);
			}

			invoker.weaponstatus[Z94S_CHAMBER]=1;
			invoker.weaponstatus[Z94S_HEAT]+=2;
			A_AlertMonsters();
		}
		goto lightdone;
	chamber_manual:
		Z94G A 1 offset(-1,34){
			if(
				invoker.weaponstatus[Z94S_CHAMBER]==2
				||invoker.weaponstatus[Z94S_MAG] < 1
			)setweaponstate("nope");
		}
		Z94G B 8 offset(-2,36)A_StartSound("weapons/libchamber");
		Z94G B 4 offset(-2,38)A_Chamber(nojam: true);
		Z94G A 1 offset(-1,34);
		goto nope;	


	firemode:
		---- A 0 {
			if(invoker.weaponstatus[Z94S_AUTO]>=2)invoker.weaponstatus[Z94S_AUTO]=0;
			else invoker.weaponstatus[Z94S_AUTO]++;
			A_WeaponReady(WRF_NONE);
		}
		goto nope;


	unloadchamber:
		Z94G B 1 offset(-1,34){
			if(
				invoker.weaponstatus[Z94S_CHAMBER]<1
			)setweaponstate("nope");
		}
		Z94G B 4 offset(-2,36)A_Chamber(nojam: true);
		Z94G B 1 offset(-2,38);
		Z94G A 1 offset(-1,34);
		goto nope;

	loadchamber:
		Z94G A 0 A_JumpIf(invoker.weaponstatus[Z94S_CHAMBER]>0,"nope");
		Z94G A 0 A_JumpIf(!countinv("HD50OMGAmmo"),"nope");
		Z94G A 1 offset(0,34) A_StartSound("weapons/pocket",CHAN_WEAPON,CHANF_OVERLAP);
		Z94G A 1 offset(2,36);
		Z94G B 4 offset(5,40);
		Z94G B 4 offset(4,39){
			if(countinv("HD50OMGAmmo")){
				A_TakeInventory("HD50OMGAmmo",1,TIF_NOTAKEINFINITE);
				invoker.weaponstatus[Z94S_CHAMBER]=2;
				A_StartSound("weapons/libchamber2",CHAN_WEAPON,CHANF_OVERLAP);
				A_StartSound("weapons/libchamber2a",6, volume: 0.7);
			}
		}
		Z94G B 7 offset(5,37);
		Z94G B 1 offset(2,36);
		Z94G A 1 offset(0,34);
		goto readyend;

	user4:
	unload:
		---- A 1;
		Z94G A 0{
			invoker.weaponstatus[0]|=Z94F_JUSTUNLOAD;
			if(
				invoker.weaponstatus[Z94S_MAG]>=0  
			){
				return resolvestate("unmag");
			}else if(
				invoker.weaponstatus[Z94S_CHAMBER]>0  
			){
				return resolvestate("unloadchamber");
			}else if(
                invoker.weaponstatus[Z94S_CHAMBER]<1
                &&invoker.weaponstatus[Z94S_MAG]<0
                &&pressingzoom()
            ){
                return resolvestate("openforrepair");
            }
			return resolvestate("nope");
		}
	reload:
		Z94G A 0{
			int inmag=invoker.weaponstatus[Z94S_MAG];
			invoker.weaponstatus[0]&=~Z94F_JUSTUNLOAD;
			if(
				//no point reloading
				inmag>=5
				||(
					//no mags to load and can't directly load chamber
					!countinv("HDZM94Mag")
					&&(
						inmag>=0
						||invoker.weaponstatus[Z94S_CHAMBER]>0
						||!countinv("HD50OMGAmmo")
					)
				)
			)return resolvestate("nope");
			else if(
				//no mag, empty chamber, have loose rounds
				inmag<0
				&&invoker.weaponstatus[Z94S_CHAMBER]<1
				&&countinv("HD50OMGAmmo")
				&&(
					pressinguse()
					||HDMagAmmo.NothingLoaded(self,"HDZM94Mag")
				)
			)return resolvestate("loadchamber");
			else if(
				invoker.weaponstatus[Z94S_MAG]>0  
			){
				//if full mag and unchambered, chamber
				if(
					invoker.weaponstatus[Z94S_MAG]>=10  
					&&invoker.weaponstatus[Z94S_CHAMBER]!=2
				){
					return resolvestate("chamber_manual");
				}				
			}return resolvestate("unmag");
		}

	unmag:
		Z94G A 1 offset(0,34);
		Z94G A 1 offset(2,36);
		Z94G B 1 offset(4,40);
		Z94G B 8 offset(8,42){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound("weapons/rifleclick2",CHAN_WEAPON,CHANF_OVERLAP);
		}
		Z94G B 16 offset(14,46){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
		}
		Z94G B 0{
			A_StartSound ("weapons/rifleload",CHAN_WEAPON,CHANF_OVERLAP);
			int magamt=invoker.weaponstatus[Z94S_MAG];
			if(magamt<0){setweaponstate("magout");return;}
			invoker.weaponstatus[Z94S_MAG]=-1;
			if(
				!PressingReload()
				&&!PressingUnload()
			){
				HDMagAmmo.SpawnMag(self,"HDZM94Mag",magamt);
				setweaponstate("magout");
			}else{
				HDMagAmmo.GiveMag(self,"HDZM94Mag",magamt);
				setweaponstate("pocketmag");
			}
		}
	pocketmag:
		Z94G B 7 offset(12,52)A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		Z94G B 0 A_StartSound("weapons/pocket");
		Z94G BB 7 offset(14,54)A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		Z94G B 0{
		}goto magout;
	magout:
		Z94G B 4{
			invoker.weaponstatus[Z94S_MAG]=-1;
			if(invoker.weaponstatus[0]&Z94F_JUSTUNLOAD)setweaponstate("reloaddone");
		}goto loadmag;

	loadmag:
		Z94G B 0 A_StartSound("weapons/pocket",CHAN_WEAPON);
		Z94G BB 7 offset(14,54)A_MuzzleClimb(frandom(-0.2,0.4),frandom(-0.2,0.8));
		Z94G B 6 offset(12,52){
			let mmm=hdmagammo(findinventory("HDZM94Mag"));
			if(mmm){
				invoker.weaponstatus[Z94S_MAG]=mmm.TakeMag(true);
				A_StartSound("weapons/rifleclick",CHAN_BODY);
				A_StartSound("weapons/rifleload",CHAN_WEAPON);
			}
		}
		Z94G B 2 offset(8,46) A_StartSound("weapons/rifleclick2",CHAN_WEAPON);
		goto reloaddone;

	reloaddone:
		Z94G B 1 offset (4,40);
		Z94G A 1 offset (2,36){
			if(
				invoker.weaponstatus[Z94S_CHAMBER]!=2
				&&invoker.weaponstatus[Z94S_MAG]>0  
			)
			setweaponstate("chamber_manual");
		}
		Z94G A 1 offset (0,34);
		goto nope;


	altfire:
		Z94G A 0 
		{
			if (invoker.weaponstatus[0] & Z94F_ISJAMMED)
			{
				A_WeaponBusy(true);
				setweaponstate("clearjam");
			}
			else if (invoker.weaponstatus[Z94S_CHAMBER] != 2)
			{
				setweaponstate("chamber_manual");
			}
		}
		goto nope;
		
	clearjam:
		Z94G B 1 offset(2, 36);
		Z94G B 1 offset(0, 34) {
			if (random(0, 100) > invoker.weaponstatus[Z94S_JAMFUCK])
			{
				setweaponstate("jamcleared");
			}
			else
			{
				invoker.weaponstatus[Z94S_JAMFUCK] -= random(1, 10);
			}
		}
		Z94G B 1 offset(4, 36);
		Z94G B 0 A_Refire("clearjam");
		goto nope;
		
	jamcleared:
		Z94G B 2 offset (0, 34) {
			invoker.weaponstatus[0] &=~ Z94F_ISJAMMED;
			A_Chamber(nojam: true);
			A_WeaponBusy(false);
		}
		Z94G B 2 offset (2, 36);
		Z94G B 2 offset (4, 38);
		Z94G B 2 offset (6, 40);
		goto nope;
	althold:
		Z94G A 0;
		goto nope;
		
	altreload:
		Z94G A 0;
		goto nope;

	spawn:
		ZM94 ABCD -1 nodelay{
			sprite=getspriteindex("ZM94A0");
			// A: +m -auto
			// B: -m -auto
			// C: +m +auto
			// D: -m +auto
			if (!(invoker.weaponstatus[0] & Z94F_STOWED))
			{
				if (invoker.weaponstatus[Z94S_MAG]<0)
					frame=3;
				else 
					frame=2;
			}
			else
			{
				if (invoker.weaponstatus[Z94S_MAG]<0)
					frame=1;
				else 
					frame=0;
			}
		}
		ZM94 ABCD -1;
		stop;
    //replacing this state so that it only stows when properly deselecting and not for sprinting/jumping
    //this also means that using the quickswap tech won't stow the gun
    deselect0bfg:
		---- A 0 A_JumpIfHealthLower(1,"deselect1big");
		---- A 0 A_JumpIfInventory("NulledWeapon",1,"deselect1bfg");
        ---- A 0 {
            invoker.weaponstatus[0] &=~ Z94F_STOWED;
        }
		---- AA 1 A_Lower(0);
		---- AA 1 A_Lower();
		---- A 1 A_Lower(1);
		---- AA 1 A_Lower(1);
		---- AA 1{
			A_MuzzleClimb(0.3,0.8);
			A_Lower(0);
		}
		---- AA 1{
			A_MuzzleClimb(-0.1,-0.4);
			A_Lower(2);
		}
		---- AAAA 1 A_Lower();
		---- A 1 A_Lower(12);
		---- A 1 A_Lower(18);
		---- A 1 A_Lower(18);
		---- A 1 A_Lower(24);
		wait;

    openforrepair:
        ---- A 2 offset(0,36);
		---- A 2 offset(4,38){
			A_StartSound("weapons/rifleclick2",CHAN_WEAPON);
			A_MuzzleClimb(-frandom(1.2,1.8),-frandom(1.8,2.4));
		}
		---- A 6 offset(9,41)A_StartSound("weapons/pocket",CHAN_WEAPON);
		---- A 8 offset(12,43)A_StartSound("weapons/zm94open1",CHAN_WEAPON,CHANF_OVERLAP);
		---- A 5 offset(10,41)A_StartSound("weapons/zm94open2",CHAN_WEAPON,CHANF_OVERLAP);
		---- A 0{
			let bbb=invoker.weaponstatus[Z94S_BREAKCHANCE];
			string msg="perfectly fine.";
			if(bbb>160)msg="like a disaster. How is it still in one piece?";
			else if(bbb>80)msg="like it's on its last legs.";
			else if(bbb>40)msg="like it's been seriously abused.";
			else if(bbb>0)msg="like it could use some adjustments.";
			A_Log("Your Sabrewolf looks "..msg,true);
			A_WeaponBusy();
		}
	readytorepair:
		---- A 1 offset(11,42){
			if(
				invoker.weaponstatus[Z94S_BREAKCHANCE]<1
				||!pressingzoom()
			){
				setweaponstate("reloaddone");
				return;
			}
			if(
				pressingfire()
				||pressingunload()
			){
				if(
					!random(0,3)
					&&invoker.weaponstatus[Z94S_BREAKCHANCE]>0
				){
					invoker.weaponstatus[Z94S_BREAKCHANCE]--;
					A_StartSound("weapons/ZM94fix",CHAN_WEAPONBODY,CHANF_OVERLAP);
					VulcRepairMsg();
				}
				if(hd_debug)A_Log("Break chance: "..invoker.weaponstatus[Z94S_BREAKCHANCE],true);
				switch(random(0,4)){
				case 1:setweaponstate("tryfix1");break;
				case 2:setweaponstate("tryfix2");break;
				case 3:setweaponstate("tryfix3");break;
				default:setweaponstate("tryfix0");break;
				}
			}
		}wait;
	tryfix0:
		---- B 2 offset(10,43)A_StartSound("weapons/ZM94tryfix",CHAN_WEAPONBODY,CHANF_OVERLAP);
		---- A 7 offset(11,42)A_MuzzleClimb(0.3,0.3,-0.3,-0.3,0.3,0.3,-0.3,-0.3);
		goto readytorepair;
	tryfix1:
		---- B 0 A_MuzzleClimb(1,1,-1,-1,1,1,-1,-1);
		---- B 2 offset(10,43)A_StartSound("weapons/ZM94belt",CHAN_WEAPONBODY,CHANF_OVERLAP);
		---- ABABABABABABBAAA 1 offset(11,44);
		goto readytorepair;
	tryfix2:
		---- B 3 offset(11,43)A_MuzzleClimb(frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1));
		---- B 7 offset(12,43)A_StartSound("weapons/ZM94tryfix2",CHAN_WEAPONBODY,CHANF_OVERLAP);
		---- A 7 offset(13,45)A_MuzzleClimb(frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1));
		---- B 12 offset(14,47)A_MuzzleClimb(frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1),frandom(-1,1));
		---- BA 2 offset(12,44)A_StartSound("weapons/ZM94tryfix2",CHAN_WEAPONBODY,CHANF_OVERLAP);
		---- B 7 offset(12,43);
		goto readytorepair;
	tryfix3:
		---- B 0 A_MuzzleClimb(1,1,-1,-1,1,1,-1,-1);
		---- B 1 offset(11,45);
		---- B 1 offset(11,48)A_StartSound("weapons/ZM94tryfix1",CHAN_WEAPONBODY,CHANF_OVERLAP);
		---- B 1 offset(12,54);
		---- B 0 A_MuzzleClimb(1,1,-1,-1,1,1,-1,-1);
		---- B 3 offset(15,58);
		---- B 2 offset(14,56);
		---- B 1 offset(12,52);
		---- B 1 offset(11,50);
		---- B 1 offset(10,48);
		goto readytorepair;

	}
	
	override void InitializeWepStats(bool idfa){
		weaponstatus[Z94S_MAG]=5;
		weaponstatus[Z94S_CHAMBER]=2;
        weaponstatus[Z94S_BREAKCHANCE]=0;
        //weaponstatus[0]|=Z94F_STOWED;
		if(!idfa && !owner){
			if(randompick(0,0,1))weaponstatus[0]|=Z94F_FRONTRETICLE;
			if(randompick(0,0,1))weaponstatus[0]|=Z94F_ALTRETICLE;
			weaponstatus[Z94S_ZOOM]=30;
			weaponstatus[Z94S_HEAT]=0;
			weaponstatus[Z94S_DROPADJUST]=75; //zeroed for 300m
		}
	}
	override void loadoutconfigure(string input){
		int altreticle=getloadoutvar(input,"altreticle",1);
		if(!altreticle)weaponstatus[0]&=~Z94F_ALTRETICLE;
		else if(altreticle>0)weaponstatus[0]|=Z94F_ALTRETICLE;

		int frontreticle=getloadoutvar(input,"frontreticle",1);
		if(!frontreticle)weaponstatus[0]&=~Z94F_FRONTRETICLE;
		else if(frontreticle>0)weaponstatus[0]|=Z94F_FRONTRETICLE;

		int bulletdrop=getloadoutvar(input,"bulletdrop",3);
		if(bulletdrop>=0)weaponstatus[Z94S_DROPADJUST]=clamp(bulletdrop,0,600);

		int zoom=getloadoutvar(input,"zoom",3);
		if(zoom>=0)weaponstatus[Z94S_ZOOM]=
			(weaponstatus[0]&Z94F_FRONTRETICLE)?
			clamp(zoom,15,40):
			clamp(zoom,4,70);
			
	}
}
enum zm69status{
	Z94F_JUSTUNLOAD 	= 1 << 0,
	Z94F_FRONTRETICLE 	= 1 << 1,
	Z94F_ALTRETICLE 	= 1 << 2,
	Z94F_UNLOADONLY 	= 1 << 3,
	Z94F_STOWED 		= 1 << 4,
	Z94F_ISJAMMED		= 1 << 5,

	Z94S_FLAGS			= 0,
	Z94S_CHAMBER		= 1,
	Z94S_MAG			= 2, //-1 is empty
	Z94S_ZOOM			= 3,
	Z94S_HEAT			= 4,
	Z94S_DROPADJUST		= 5,
	Z94S_JAMFUCK		= 6,
	Z94S_BREAKCHANCE	= 7,
    Z94S_AUTO           = 8, //2 is burst, 2-4 counts ratchet
};

class ZM94Random : IdleDummy
{
	
	states
	{
		spawn:
			TNT1 A 0 nodelay {
				let lll = ZM94Rifle(spawn("ZM94Rifle", pos, ALLOW_REPLACE));
				
				if(!lll)
					return;
					
				lll.special=special;
				
				lll.vel=vel;
				
				if (!random(0, 3))
				{
					lll.weaponstatus[0] |= Z94F_STOWED;
				}
				
				lll.vel = vel;
				
				actor otherspawn;
				if (!random(0, 2))
				{
					otherspawn = spawn("HD50OMGBoxPickup",pos+(5,0,0),ALLOW_REPLACE);
					otherspawn.vel = vel;
				}
				otherspawn = spawn("HDZM94Mag",pos+(8,0,0),ALLOW_REPLACE);
				otherspawn.vel = vel;
			}
			stop;
	}
}



class HDZM94Mag : HDMagAmmo
{
	default
	{
		//$Category "Ammo/Hideous Destructor/"
		//$Title ".50 OMG Magazine"
		//$Sprite "OMG5A0"

		hdmagammo.maxperunit 5;
		hdmagammo.roundtype "HD50OMGAmmo";
		hdmagammo.roundbulk ENC_50OMG_LOADED*0.75;
		hdmagammo.magbulk ENC_ZM94MAG_EMPTY;
		hdpickup.refid HDLD_ZM94MAG;
		tag "ZM94 magazine";
		inventory.pickupmessage "Picked up a 5-round .50 OMG magazine.";
		scale 0.8;
	}
	override string,string,name,double getmagsprite(int thismagamt)
	{
		string magsprite=(thismagamt>0)?"OMG5A0":"OMG5B0";
		return magsprite,"OGBLA3A7","HD50OMGAmmo",1.7;
	}
	override void GetItemsThatUseThis(){
		itemsthatusethis.push("ZM69Rifle");
	}
	states{
	spawn:
		OMG5 A -1;
		stop;
	spawnempty:
		OMG5 B -1{
			brollsprite=true;brollcenter=true;
			roll=randompick(0,0,0,0,2,2,2,2,1,3)*90;
		}stop;
	}
}

class HDZM94EmptyMag:IdleDummy{
	override void postbeginplay(){
		super.postbeginplay();
		HDMagAmmo.SpawnMag(self,"HDZM94Mag",0);
		destroy();
	}
}
